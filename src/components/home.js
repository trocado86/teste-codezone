import React, { Component } from 'react';
import { Button, Container, Col, Row, ProgressBar } from 'react-bootstrap';
import { FaArrowLeft, FaArrowRight,FaFacebookSquare, FaAt, FaInstagram, FaYoutube } from 'react-icons/fa';
import './../styles/global.scss';

import youtube from './../imgs/youtube.jpg'; 
import wiki from './../imgs/wiki.jpg'; 
import order from './../imgs/newOrder.jpeg'; 
import kirrask from './../imgs/kirrask.jpg'; 

class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedSlide: 0,
            slideSize: 0,
            timer: 0,
            timerSize: 5,
            leftStart:0,
            leftEnd:0,
            imgs: [
                {
                    src: 'http://www.21nodado.com.br/static/media/char06_full.74ce6c53.jpg',
                    name: 'Kirrask',
                    class: 'show'
                },
                {
                    src: 'http://www.21nodado.com.br/static/media/char03_full.c7da0f7c.jpg',
                    name: 'Dorian',
                    class: 'hidden'
                },
                {
                    src: 'http://www.21nodado.com.br/static/media/char01_full.34d14e54.jpg',
                    name: 'Sorian',
                    class: 'hidden'
                }
            ]
        }
        this.nextSlide = this.nextSlide.bind(this);
        this.prevSlide = this.prevSlide.bind(this);
        this.getImageSize = this.getImageSize.bind(this);
        this.selectSlide = this.selectSlide.bind(this);
        this.dragEventStart =this.dragEventStart.bind(this);
    }
    nextSlide(selectedSlide) {
        this.state.imgs[selectedSlide].class = "hidden"
        if (this.state.imgs.length - 1 == selectedSlide) {
            selectedSlide = 0
        } else {
            selectedSlide = selectedSlide + 1
        }
        this.state.imgs[selectedSlide].class = 'show'
        clearTimeout(this.timeInterval);
        this.setState({
            imgs: this.state.imgs,
            selectedSlide: selectedSlide,
            timer: 0
        })
        this.handleTime()
    }
    prevSlide(selectedSlide) {
        this.state.imgs[selectedSlide].class = "hidden"
        if (selectedSlide == 0) {
            selectedSlide = this.state.imgs.length - 1
        } else {
            selectedSlide = selectedSlide - 1
        }
        this.state.imgs[selectedSlide].class = 'show'
        clearTimeout(this.timeInterval);
        this.setState({
            imgs: this.state.imgs,
            selectedSlide: selectedSlide,
            timer: 0
        })
        this.handleTime()
    }

    selectSlide(index){
        this.state.imgs[this.state.selectedSlide].class = "hidden"
        this.state.imgs[index].class = "show"
        clearTimeout(this.timeInterval);
        this.setState({
            imgs: this.state.imgs,
            selectedSlide: index,
            timer: 0
        })
        this.handleTime()
    }

    timeHandleBar(){
        if(this.state == undefined){
            this.state= {timer:0}
        }  
        if(this.state.timer == 5){
            this.nextSlide(this.state.selectedSlide)
            clearTimeout(this.timeInterval);
            this.setState({
                timer: 0
            })
            this.handleTime()
        }
        else{
            this.state.timer++
            this.setState({
                timer: this.state.timer
            })
        }

    }

    getImageSize() {
        var slide0 = document.getElementById('img-slide-0')
        this.setState({
            slideSize: slide0.height + 'px'
        })

    }

    handleTime(){
        this.timeInterval = setInterval(
            () => this.timeHandleBar(),
            1000
        );
    }

    componentDidMount() {
        window.addEventListener('resize', this.getImageSize)
        this.handleTime()
    }

    dragEventStart(e){
        if(this.state.leftStart == 0){
            this.setState({
                leftStart: e.screenX
            })
        }
    }
    dragEventEnd(e){
        this.setState({
            leftStart: 0
        })
        if(this.state.leftStart > e.screenX){
            this.nextSlide(this.state.selectedSlide)        
        }else{
            this.prevSlide(this.state.selectedSlide)
        }
    }

    render() {
        return (
            <div>
                <header className="App-header" style={{ backgroundColor: 'grey' }} >
                    <Container style={{ position: 'absolute', top: '0' }}>
                        <Row>
                            <Col md={12} sm={12} lg={12} className="box-1">
                                <img src="http://www.21nodado.com.br/static/media/logo-no-text.24b71b87.png"/>

                            </Col>
                        </Row>
                        <Row>
                            <Col md={{ span: 2 }} sm={12}>
                                <Row>
                                    <Col md={{ span: 12 }} className="container-slider" style={{ padding: '0', height: this.state.slideSize }} 
                                        
                                    >

                                        <button className="slide-btn" onClick={(e) => this.prevSlide(this.state.selectedSlide)}><FaArrowLeft /></button>
                                        {this.state.imgs.map((value, index) => (
                                            <div className={'wrapper '} key={index}>
                                                <div className={value.class + " slide"}>
                                                    <img onLoad={this.getImageSize.bind(this)} 
                                                    onTouchStart={(e) => this.dragEventStart(e)}
                                                    onTouchEnd={(e) => this.dragEventEnd(e)}
                                                    onDragEnter={(e) => this.dragEventStart(e)}
                                                    onDragLeave={(e) => this.dragEventEnd(e)}
                                                        id={'img-slide-' + index} src={value.src}
                                                        style={{ width: '100%' }}
                                                    />
                                                </div>
                                            </div>
                                        ))}
                                        <div className="sliders-select">
                                            {this.state.imgs.map((value, index) => (
                                                <div className="btn-select-sld" key={index}
                                                onClick={(e) => this.selectSlide(index)}
                                                style={{backgroundColor: this.state.selectedSlide == index ? 'red' : 'rgba(100,100,100, .7)'}}>

                                                </div>
                                            ))}
                                        </div>
                                        <ProgressBar animated  className="sld-progress-bar" now={this.state.timer*20} />
                                        {/* <ProgressBar animated  className="sld-progress-bar" now={this.state.timer/this.state.timeInterval*100} label={`${this.state.timer}%`} /> */}

                                        <button className="slide-btn right" onClick={(e) => this.nextSlide(this.state.selectedSlide)} ><FaArrowRight /></button>
                                    </Col>
                                    <Col md={{ span: 12 }} className="box-3">
                                        <div>
                                            <h5 className="title">Parceiro:</h5>
                                            <img src={order}/>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={{ span: 10 }} sm={12}>
                                <Row>
                                    <Col md={12} sm={12} lg={6} className="box-4">
                                        <img src={youtube}/>
                                    </Col>
                                    <Col md={12} sm={12} lg={6} className="box-5">
                                       <div className="text"> 
                                       <h3 style={{color: "#740101"}}>Quem Somos</h3>
                                       Somos um grupo que produz conteúdo sobre RPG e nosso primeiro projeto é apresentar uma campanha de Pathfinder 2. Todos nós da “21 no dado” jogamos RPG faz muito tempo e, com o fortalecimento do cenário e o crescimento dos canais de transmissão de mesas, pensamos em dividir nossa campanha com você. Nossa ideia é trazer uma experiência de jogo balanceada, com base nas regras e estratégias de combate, mas sem perder o foco na interpretação.
                                        </div> 
                                    </Col>
                                    <Col md={12} sm={12} lg={12} className="box-6">
                                        <img src={wiki}/>
                                    </Col>
                                    <Col md={5} sm={12} lg={5} className="box-7">
                                        " É preciso muita audácia para enfrentarmos os nossos inimigos, mas igual audácia para defendermos os nossos amigos." 
                                        <div>Harry Potter</div>
                                    </Col>
                                    <Col md={5} sm={8} lg={5} xs={8} className="box-8">
                                        " Relaxa essa criança é inofencivas, acha que ela vai fazer o que?"
                                        <div>Kirrask - Última Frase</div>
                                    </Col>
                                    <Col md={2} sm={4} lg={2} xs={4} className="box-9">
                                        <img src={kirrask}/>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} sm={12} lg={12} className="box-10">
                            <a href="mailto:contato@21nodado.com.br?Subject=21contato%20again"><span className="email"><FaAt className="margin-r-10px email" style={{fontSize:'2em'}}/> <span className="mobile-hidden" style={{marginRight:'15px'}}>21nodado@gmail.com</span></span></a>
                            <a target="_blank" href="http://fb.me/21nodado"><FaFacebookSquare className="margin-r-10px facebook" style={{fontSize:'2em'}}/>  </a>
                            <a target="_blank" href="http://instagr.com/21nodado"><FaInstagram className="margin-r-10px instagram" style={{fontSize:'2em'}}/> </a>
                            <a target="_blank" href="https://www.youtube.com/channel/UCk2E7e8xWvJjhAJZdW7eOug"><FaYoutube className="margin-r-10px youtube" style={{fontSize:'2em'}}/></a> 
                            </Col>
                        </Row>
                    </Container>
                </header>
            </div>
        )
    }
}

export default function HomePage(props) {
    return (
        <Home />
    )
}
