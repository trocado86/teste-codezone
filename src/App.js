import React from 'react';
// import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import HomePage from './components/home'

import {
  BrowserRouter,
  Route,
  Switch,
  Link
} from 'react-router-dom'

function App() {
  return (
    <BrowserRouter>
      <div>
        <Switch>           
          <Route path='/' exact component={HomePage} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
